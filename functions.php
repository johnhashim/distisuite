<?php
/**
 * DistiSuite functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package DistiSuite
 */

if ( ! function_exists( 'distisuite_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function distisuite_setup() {
		/**
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on DistiSuite, use a find and replace
		 * to change 'distisuite' to the name of your theme in all the template files.
		 * You will also need to update the Gulpfile with the new text domain
		 * and matching destination POT file.
		 */
		load_theme_textdomain( 'distisuite', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/**
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/**
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		add_image_size( 'full-width', 1920, 1080, false );
		add_image_size( 'refference-image', 120, 125, false );

		// Register navigation menus.
		register_nav_menus(
			 array(
				 'primary'     => esc_html__( 'Primary Menu', 'distisuite' ),
				 'mobile'      => esc_html__( 'Mobile Menu', 'distisuite' ),
				 'secondary'   => esc_html__( 'secondry Menu', 'distisuite' ),
				 'sidebar'     => esc_html__( 'sidebar Menu', 'distisuite' ),
				 'contactmenu' => esc_html__( 'Top Contact Menu', 'distisuite' ),
				 'footermenu'  => esc_html__( 'Footer Menu', 'distisuite' ),

			 )
			);

		/**
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			 'html5', array(
				 'search-form',
				 'comment-form',
				 'comment-list',
				 'gallery',
				 'caption',
			 )
			);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background', apply_filters(
			 'distisuite_custom_background_args', array(
				 'default-color' => 'ffffff',
				 'default-image' => '',
			 )
			)
			);

		// Custom logo support.
		add_theme_support(
			 'custom-logo', array(
				 'height'      => 250,
				 'width'       => 500,
				 'flex-height' => true,
				 'flex-width'  => true,
				 'header-text' => array( 'site-title', 'site-description' ),
			 )
			);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );
	}
endif;
/**
 * Distisuite_setup
 */
add_action( 'after_setup_theme', 'distisuite_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function distisuite_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'distisuite_content_width', 640 );
}
add_action( 'after_setup_theme', 'distisuite_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function distisuite_widgets_init() {

	// Define sidebars.
	$sidebars = array(
		'sidebar-1'    => esc_html__( 'Sidebar 1', 'distisuite' ),
		'menu-sidebar' => esc_html__( 'Menu Sidebar', 'distisuite' ),
	);

	// Loop through each sidebar and register.
	foreach ( $sidebars as $sidebar_id => $sidebar_name ) {
		register_sidebar(
			 array(
				 'name'          => $sidebar_name,
				 'id'            => $sidebar_id,
				 'description'   => /* translators: the sidebar name */ sprintf( esc_html__( 'Widget area for %s', 'distisuite' ), $sidebar_name ),
				 'before_widget' => '<aside class="widget %2$s">',
				 'after_widget'  => '</aside>',
				 'before_title'  => '<h2 class="widget-title">',
				 'after_title'   => '</h2>',
			 )
			);
	}

}
add_action( 'widgets_init', 'distisuite_widgets_init' );
/**
 * Custom excerpt lenght.
 */


add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

/**
 * Custom excerpt lenght.
 *
 * @param return $limit  comment.
 * @return tag $excerpt.
 */
function footer_excerpt( $limit ) {
	$excerpt = explode( ' ', get_the_excerpt(), $limit );
	if ( count( $excerpt ) >= $limit ) {
	  array_pop( $excerpt );
	  $excerpt = implode( ' ', $excerpt ) . '';
	} else {
	  $excerpt = implode( ' ', $excerpt );
	}
	$excerpt = preg_replace( '`[[^]]*]`', '', $excerpt );
	return $excerpt;
  }

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load styles and scripts.
 */
require get_template_directory() . '/inc/scripts.php';

/**
 * Load custom ACF features.
 */
require get_template_directory() . '/inc/acf.php';

/**
 * Load custom filters and hooks.
 */
require get_template_directory() . '/inc/hooks.php';

/**
 * Load custom queries.
 */
require get_template_directory() . '/inc/queries.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer/customizer.php';

/**
 * Scaffolding Library.
 */
require get_template_directory() . '/inc/scaffolding.php';

/**
 * Create theme optoins page
 */
if ( function_exists( 'acf_add_options_page' ) ) {

	acf_add_options_page(
		array(
			'page_title' => 'Distisuite General Settings',
			'menu_title' => 'Distisuite Settings',
			'menu_slug'  => 'distisuite-general-settings',
			'capability' => 'edit_posts',
			'redirect'   => false,
		)
		);
}

add_filter( 'gform_field_value', 'populate_fields', 10, 3 );
/**
 * Removing review tabs
 *
 * @param return $value  comment.
 * @param return $field comment.
 * @param return $name comment.
 * @return return $values  comment.
 */
function populate_fields( $value, $field, $name ) {

$values = array(
	'product_number' => '',
	'manufacturer'   => '',
);

return isset( $values[ $name ] ) ? $values[ $name ] : $value;
}


/**
 * Adding woocommerce support
 */
function mytheme_add_woocommerce_support() {
add_theme_support( 'woocommerce' );
add_theme_support( 'wc-product-gallery-zoom' );
add_theme_support( 'wc-product-gallery-lightbox' );
add_theme_support( 'wc-product-gallery-slider' );

}
add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );

add_action( 'init', 'ds_single_product_mods' );
/**
 * Seting single product modes
 */
function ds_single_product_mods() {
/**
 * Remove Breadcrumbs
 */
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
/**
 * Remove Product Sidebar
 */
remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );
}


add_action( 'woocommerce_after_shop_loop_item', 'add_a_custom_button', 10 );
	/**
	 * Add view product
	 *
	 * @return return $product.
	 */
	function add_a_custom_button() {
	global $product;
	// Not for variable and grouped products that doesn't have an "add to cart" button.
	if ( $product->is_type( 'variable' ) || $product->is_type( 'grouped' ) ) {
		return;
	}
	// Output the custom button linked to the product.
	echo '<a class="button  btn-details" href="' . esc_attr( $product->get_permalink() ) . '">' . esc_textarea( ( 'View details' ) ) . '</a>';
	}



add_filter( 'woocommerce_product_add_to_cart_text', 'customizing_add_to_cart_button_text', 10, 2 );
add_filter( 'woocommerce_product_single_add_to_cart_text', 'customizing_add_to_cart_button_text', 10, 2 );
/**
 * Replace read more buttons for out of stock items
 *
 * @param button  $button_text comment.
 * @param product $product comment.
 * @return return $button_text.
 */
function customizing_add_to_cart_button_text( $button_text, $product ) {
		global $product;
		$sold_out     = __( 'Get A Quote', 'woocommerce' );
		$availability = $product->get_availability();
		$stock_status = $availability['class'];
// For all other cases (not a variable product on single product pages).
if ( ! $product->is_type( 'variable' ) && ! is_product() ) {
		if ( 'out-of-stock' === $stock_status ) {
			$button_text = $sold_out;
			}
} else {
		$button_text;
}
return $button_text;
}


add_filter( 'woocommerce_product_add_to_cart_url', 'out_of_stock_read_more_url', 50, 2 );
/**
 * Replace read more buttons for out of stock items
 *
 * @param link    $link comment.
 * @param product $product comment.
 * @return return $product_mfg.
 */
	function out_of_stock_read_more_url( $link, $product ) {
	// Only for "Out of stock" products.
	if ( $product->get_stock_status() == 'outofstock' ) {

		// Here below we change the link.
		$link = home_url( '/request-a-quote?productnumber=' . get_the_title() . '&manufacturer=' . $product_mfg->name );
	}
	return $link;
	}


/**
 * Plus and minus button function
 */
function distisuite_add_script_to_footer() {
	if ( ! is_admin() ) {
		?>
<script>
	jQuery(document).ready(function($){

$(document).on('click', '.plus', function(e) { // replace '.quantity' with document (without single quote)
	$input = $(this).prev('input.qty');
	var val = parseInt($input.val());
	var step = $input.attr('step');
	step = 'undefined' !== typeof(step) ? parseInt(step) : 1;
	$input.val( val + step ).change();
});
$(document).on('click', '.minus',  // replace '.quantity' with document (without single quote)
	function(e) {
	$input = $(this).next('input.qty');
	var val = parseInt($input.val());
	var step = $input.attr('step');
	step = 'undefined' !== typeof(step) ? parseInt(step) : 1;
	if (val > 0) {
		$input.val( val - step ).change();
	} 
});
	$('div.woocommerce').on('click', 'input.minus', function(){ 
		$("[name='update_cart']").trigger("click"); 
	}); 
	$('div.woocommerce').on('click', 'input.plus', function(){ 
		$("[name='update_cart']").trigger("click"); 
	}); 
});


</script>
<?php
	}
}

/**
 * Plus and minus button
 */
add_action( 'wp_footer', 'distisuite_add_script_to_footer' );

// Override for the PHP version 7.2.
if ( ! function_exists( 'prefix_woocommerce_csv_product_import_valid_filetypes' ) ) :
/**
 * Override for the PHP version 7.2.
 *
 * @param defaults $defaults comment.
 */
	function prefix_woocommerce_csv_product_import_valid_filetypes( $defaults ) {
		$defaults['csv'] = 'text/plain';
		return $defaults;
	}
	add_filter( 'woocommerce_csv_product_import_valid_filetypes', 'prefix_woocommerce_csv_product_import_valid_filetypes' );
endif;
