<?php
/**
 *
 * Template name: Reference page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package DistiSuite
 */

get_header(); ?>

	<div class="primary content-area">
		<main id="main" class="site-main">
			<div class="inner-grid">

			<header class="woocommerce-products-header">
				<?php
				$term = get_field( 'manufacture_taxonomy' )[0];
				?>
				<div class="company-logo">
					<a href="<?php the_field( 'product_url' ); ?>"><img class="logo" src="<?php the_field( 'featured_image', $term ); ?>"></a>
				</div>
				<div class="manufacturer-content">
					<div class= "manufacture-name">
					<a class="woocommerce-products-header__title page-title" href="<?php echo esc_url( get_term_link( $term ) ); ?>"><?php echo $term->name; ?></a>
					<a class="view-parts" href="<?php echo esc_url( get_term_link( $term->term_taxonomy_id ) ); ?>">View All Parts</a>
					</div>
					<div class="manufacture-details">
					<a href= "<?php the_field( 'website_url', $term ); ?> ">visit  <?php echo esc_html( $term->name ); ?> website</a>
					<p><?php echo esc_html( $term->description ); ?></p>
					</div>
				</div>
				
			</header>
			<div>
				<?php
					// If the page is password protected...
					if ( post_password_required() ) :
						get_template_part( 'template-parts/content', 'password-protected' );
					else :
						distisuite_display_content_blocks();
					endif;
				?>
			</div>
			
			</div>
		</main><!-- #main -->
	</div><!-- .primary -->

<?php get_footer(); ?>
