jQuery( document ).ready( function() {
	jQuery( '.section-title' ).on( 'click', function() {
		jQuery( this ).toggleClass( 'open' );
		jQuery( this ).siblings().slideToggle();
	} );
} );

//sidebar-menu js
jQuery( document ).ready( function() {
	jQuery( '#menu-sidebar-menu li:has(ul.sub-menu) > a' ).on( 'click', function( e ) {
		e.preventDefault();
		jQuery( this ).next().slideToggle();
		jQuery( this ).parent( '.menu-item-has-children' ).toggleClass( 'active' );

	} );

} );
