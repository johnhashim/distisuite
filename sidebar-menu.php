<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package DistiSuite
 */

if ( ! is_active_sidebar( 'menu-sidebar' ) ) {
	return;
}
?>

<aside class="secondary widget-area col-l-4" role="complementary">
	<?php dynamic_sidebar( 'menu-sidebar' ); ?>
</aside><!-- .secondary -->
