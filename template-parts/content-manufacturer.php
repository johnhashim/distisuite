<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package DistiSuite
 */

// If the page is password protected...
if ( post_password_required() ) :
 get_template_part( 'template-parts/content', 'password-protected' );
else :
distisuite_display_content_blocks();
endif;

?>

<div class="frontpage-category">
<?php dynamic_sidebar( 'menu-sidebar' ); ?>

<div class="col-l-8 featured-product primary">
	<div class="heading-ds">
	<h2 class="title">manufacturers</h2>
	</div>
	<div class="invetory">
	<div class="manufacturer  ">
		
		<?php
		/**
		 * WP_Term_Query arguments
		 */
		$args = array(
			'taxonomy'               => array( 'manufacturers' ),
		);

		/**
		 * The Term Query
		 */
		$term_query = new WP_Term_Query( $args );
		
		/**
		 * The Loop
		 */
		if ( ! empty( $term_query ) && ! is_wp_error( $term_query ) ) {
		foreach ( $term_query->get_terms() as $term ) {
		// The $term is an object, so we don't need to specify the $taxonomy.
		$term_link = get_term_link( $term );
		


		// If there was an error, continue to the next term.
		if ( is_wp_error( $term_link ) ) {
			continue;
		}
		/**
		 * The Loop
		 */
		?>

		<div class="card-container" >  
			<div class="company-logo cells">
				 <?php
				 // If Manufacture landing page exist add link if not default.
				 if (get_field( 'featured_information', $term )) {
					 ?>
					<a href="<?php the_field( 'featured_information', $term ); ?>"><img class="logo" src="<?php the_field( 'featured_image', $term ); ?>" alt="<?php the_field( 'featured_image', $term ); ?>"></a>
				 <?php 
				 } else {
					 ?>
					<a href="<?php echo esc_url( $term_link ); ?>"><img class="logo" src="<?php the_field( 'featured_image', $term ); ?>" alt="<?php the_field( 'featured_image', $term ); ?>"></a>
					<?php  
				}
				 ?>
			
			</div>  
		</div> 

		<?php

		}
}

		?>
	</div>
	</div>
</div>
