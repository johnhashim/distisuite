<?php
/**
 *  The template used for displaying fifty/fifty text/text.
 *
 * @package DistiSuite
 */

// Start a <container> with a possible media background.
distisuite_display_block_options( array(
	'container' => 'section', // Any HTML5 container: section, div, etc...
	'class'     => 'content-block grid-container form-selection', // The container class.
) );
?>
	<div class="inner-grid">

<div class="the-form">
	<?php
		$form = get_sub_field( 'gravit_form' );
		gravity_form( $form, false, true, false, '', true, 1 );
	?>
 </div>

	</div><!-- .grid-x -->
</section><!-- .fifty-text-only -->
