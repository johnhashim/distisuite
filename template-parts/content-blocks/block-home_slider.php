<?php
/**
 * The template used for displaying a recent posts block.
 *
 * This block will either display all recent posts or posts
 * from a specific category. The amount of posts can be
 * limited through the admin.
 *
 * @package DistiSuite
 */

distisuite_display_block_options(
	array(
		'container' => 'section', // Any HTML5 container: section, div, etc...
		'class'     => 'content-block grid-container home-slider', // The class of the container.
	)
   );
   ?>
<div class="disti-slider">
<?php
$slider_id = get_sub_field( 'slider' );

if ( function_exists( 'soliloquy' ) ) {
	soliloquy( $slider_id );
}
?>
</div>
</section>
