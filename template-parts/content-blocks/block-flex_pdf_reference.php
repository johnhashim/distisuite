<?php
/**
 *  The template used for displaying fifty/fifty text/text.
 *
 * @package DistiSuite
 */

// Set up fields.

// Start a <container> with a possible media background.
distisuite_display_block_options(
	 array(
		 'container' => 'section', // Any HTML5 container: section, div, etc...
		 'class'     => 'content-block grid-container flex-pdf', // Container class.
	 )
	);
?>	
<div class="Products">
<?php

if ( have_rows( 'pdf_reference' ) ) :

// Loop through the rows of data.
while ( have_rows( 'pdf_reference' ) ) :
		the_row();

        // Display a sub field value.
        ?>
        <div class="product">
        <?php
        $pdf = get_sub_field( 'pdf' );
        $image = get_sub_field( 'pdf_icon' );
        $size  = 'refference-image'; 
        ?>

        <a href="<?php echo $pdf; ?>">
          <?php echo wp_get_attachment_image( $image, $size ); ?>
        </a>
            <?php
            the_sub_field( 'pdf_description' );
            ?>
        </div>
            
<?php


endwhile;
endif;
?>
</div>	
</section><!-- .fifty-text-media -->
