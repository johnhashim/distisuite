<?php
/**
 * The template used for displaying a generic Faqs block.
 *
 * @package DistiSuite
 */

// Set up fields.
distisuite_display_block_options(
	array(
		'container' => 'section', // Any HTML5 container: section, div, etc...
		'class'     => 'faqs', // Container class.
	)
);
?>
<div class=" faqs-content inner-grid">
 <div class="faqs-container">
 <div class="header">
  <h3 class="title"><?php the_sub_field( 'faqs_header' ); ?></h3>
</div>
<?php
// Check if the repeater field has rows of data.
if ( have_rows( 'faq' ) ) :

// loop through the rows of data.
while ( have_rows( 'faq' ) ) :
the_row();

?>
<div class="expander-container">
 <div class="expander">
 <div class="section-title closed 
 <?php
 if ( get_row_index() == 1 ) {
echo 'open row-1'; }
?>
">
<div class="plusminus"></div>
<h2 class="question"><?php the_sub_field( 'description_header' ); ?></h2>
</div>
<div class="section-content">
<?php the_sub_field( 'summary' ); ?>
</div>
</div><!-- .expander -->
</div><!-- .expander-container -->
 <?php
endwhile;
endif;


?>
</div>


<div class="faq-contact wrap">
	<div class="faq-footer">
		<p>If you still have questions </P>
	</div>
	<div class="call-to-action">
		<a href="<?php the_sub_field( 'enquiry_page_link' ); ?>" class="button info-btn">Contact Us For More Info</a>
	</div>
</div>
</section>
