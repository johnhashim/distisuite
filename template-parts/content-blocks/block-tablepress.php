<?php
/**
 *  The template used for displaying fifty/fifty text/text.
 *
 * @package DistiSuite
 */

// Set up fields.

// Start a <container> with a possible media background.
distisuite_display_block_options( array(
	'container' => 'section', // Any HTML5 container: section, div, etc...
	'class'     => 'content-block grid-container table-block', // The container class.
) );
?>
	<div class="table <?php echo esc_attr( $animation_class ); ?>">
		<div class="">
			<?php
				the_sub_field( 'table' );
			?>
		</div>

	</div><!-- .grid-x -->
</section><!-- .fifty-text-only -->
