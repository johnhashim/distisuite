<?php
/**
 *  The template used for displaying fifty/fifty text/text.
 *
 * @package DistiSuite
 */

// Start a <container> with a possible media background.
distisuite_display_block_options( array(
	'container' => 'section', // Any HTML5 container: section, div, etc...
	'class'     => 'content-block grid-container contact-info', // The container class.
) );

?>

<div class="inner-grid">
  <h2 class="page-title"><?php echo get_the_title(); ?></h2>
	<div class="information">
		<?php
				if ( have_rows( 'contact_info' ) ) :

				while ( have_rows( 'contact_info' ) ) :
				the_row();
				?>
				<div class="detatils">
						<h2 class="contact-title"><?php the_sub_field( 'contact_title' ); ?></h2>
						

					<?php if ( get_sub_field( 'operating_details' ) ) : ?>

					<ul class="info">

						<?php while ( has_sub_field( 'operating_details' ) ) : ?>

							<li><?php the_sub_field( 'row' ); ?></li>

						<?php endwhile; ?>

					</ul>

		<?php endif; ?>
				</div>
		<?php
		endwhile;
		endif;
		?>
	</div>
</div><!-- .grid-x -->
</section><!-- .fifty-text-only -->
