<?php
/**
 *  The template used for displaying fifty/fifty text/text.
 *
 * @package DistiSuite
 */

// Set up fields.

// Start a <container> with a possible media background.
distisuite_display_block_options(
	 array(
		 'container' => 'section', // Any HTML5 container: section, div, etc...
		 'class'     => 'content-block grid-container flex-product', // Container class.
	 )
	);
?>	
<div class="Products">
<?php

if ( have_rows( 'product_reference' ) ) :

// Loop through the rows of data.
while ( have_rows( 'product_reference' ) ) :
		the_row();

        // Display a sub field value.
        ?>
        <div class="product">
        <?php
        $image = get_sub_field( 'product_image' );
        $size  = 'refference-image'; // (thumbnail, medium, large, full or custom size)

            echo wp_get_attachment_image( $image, $size ); 
            the_sub_field( 'product_description' );
            ?>
        </div>
            
<?php


endwhile;
endif;
?>
</div>	
</section><!-- .fifty-text-media -->
