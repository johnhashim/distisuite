<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package DistiSuite
 */

// If the page is password protected...
if ( post_password_required() ) :
 get_template_part( 'template-parts/content', 'password-protected' );
else :
distisuite_display_content_blocks();
endif;

?>

<div class=" featured-product">
<div class="manufacturer inner-grid">
		<div class="header">
		<h2 class="title"> Manufacturers</h2>
		</div>
	<?php
	/**
	 * WP_Term_Query arguments
	 */
	$args = array(
		'taxonomy'               => array( 'manufacturers' ),
	);

	/**
	 * The Term Query
	 */
	$term_query = new WP_Term_Query( $args );
	if ( ! empty( $term_query ) && ! is_wp_error( $term_query ) ) {
	foreach ( $term_query->get_terms() as $term ) {
	// The $term is an object, so we don't need to specify the $taxonomy.
	$term_link = get_term_link( $term );

	// If there was an error, continue to the next term.
	if ( is_wp_error( $term_link ) ) {
		continue;
	}
	/**
	 * The Loop
	 */
	?>

	<div class="manufacturer-list" >  
	<div class="company-logo"> 
		<a href="<?php echo esc_url( $term_link ); ?>"><img class="logo" src="<?php the_field( 'featured_image', $term ); ?>" alt="<?php the_field( 'featured_image', $term ); ?>"></a>
	</div>
	<div class="manufacturer-contents">
	   <div class="description">
		<h2 class="title">
		<a href="<?php echo esc_url( $term_link ); ?>">
		<?php
		echo esc_html( $term->name );
		?>
		</a>
		</h2>
		<a href="<?php the_field( 'website_url', $tax_id ); ?>"><?php the_field( 'website_url', $term ); ?></a>
		<div>
		<?php
		echo esc_html( $term->description );
		?>
		</div>
		</div>
		<div class="button-section">
			<a class="button info-btn" href="<?php the_field( 'featured_information', $term ); ?>">Featured Information</a>
			<a class="button linecard-btn" href="<?php echo esc_url( $term_link ); ?>">View All parts</a>
		</div>
	</div>
	</div> 

	<?php

	}
}

	?>
	</div>
</div>
