<?php
/**
 * Template Name : blog posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package DistiSuite
 */

?>
<article class="manufacturer-news" <?php post_class(); ?> >
<div class="featured-img">
<?php if ( has_post_thumbnail() ) : ?>
 <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
  <?php the_post_thumbnail(); ?>
 </a>
<?php endif; ?>
</div>
<div class="blog-content">
	<header class="entry-header">
		<?php
		if ( is_single() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;
		if ( 'post' === get_post_type() ) :
		?>
		<div class="entry-meta">
			<?php distisuite_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			echo esc_html( footer_excerpt( 40 ) );
		?>
		<a class="moretag" href="<?php the_permalink(); ?>" rel="bookmark">[Read More]</a>
	</div><!-- .entry-content -->
</div>
</article><!-- #post-## -->
