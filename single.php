<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package DistiSuite
 */

get_header(); ?>

	<div class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'single' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>
<div class="request-us">
	 <a class='button linecard-btn' href="<?php the_field( 'contact_page_url', 'option' ); ?>">Contact Us For More Info</a>
</div>
		</main><!-- #main -->
	</div><!-- .primary -->

<?php get_footer(); ?>
